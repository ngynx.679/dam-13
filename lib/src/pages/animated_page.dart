import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AnimatedContainerState();
  }
}

class _AnimatedContainerState extends State<AnimatedContainerPage> {
  bool selected = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animated Container'),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Container(
                child: Text('Cambiar Valor'),
              ),
              onPressed: () {
                setState(() {
                  selected = !selected;
                });
              },
            ),
            Text (''),
            AnimatedContainer(
              width: selected ? 350.0 : 150.0,
              height: selected ? 150.0 : 350.0,
              color: selected ? Colors.red : Colors.yellow,
              alignment:
                  selected ? Alignment.center : AlignmentDirectional.center,
              duration: Duration(seconds: 3),
              curve: Curves.fastOutSlowIn,
              child: FlutterLogo(size: 95),
            ),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
              child: Icon(Icons.arrow_back),
              backgroundColor: Colors.red,
              onPressed: () {
                Navigator.of(context).pop(true);
              }),
        ],
      ),
    );
  }
}
