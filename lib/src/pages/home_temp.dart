import 'package:flutter/material.dart';
import 'package:sesion13/src/providers/menu_provider.dart';
import 'package:sesion13/src/utils/icono_string_util.dart';
//TextStyle styleText = TextStyle(color: Colors.red, fontSize: 25.0);

class HomePageTemp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
        backgroundColor: Colors.blue,
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(children: _listaItems(snapshot.data, context));
      },
    );
  }

  List<Widget> _listaItems(List<dynamic> data, context) {
    final List<Widget> opciones = [];

    data.forEach((opt) {
      final widgetTemp = ListTile(
        title: Text(opt['text']),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.arrow_forward_ios),
        onTap: () {
          print(opt['ruta']);
          Navigator.pushNamed(context, opt['ruta']);

        },
      );

      opciones..add(widgetTemp)..add(Divider());
    });
    return opciones;
  }
}
