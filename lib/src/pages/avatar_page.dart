import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar'),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Inicio Ejemplo Avatar
            CircleAvatar(
              radius: 50,
              backgroundColor: Colors.blue,
              child: Text('AKW', style: TextStyle(fontSize: 35)),
            ),
            Text(" "),
            CircleAvatar(
              radius: 75,
              backgroundImage: NetworkImage('https://es.calcuworld.com/wp-content/uploads/sites/2/2018/04/cuanto-mide-el-universo-entero.jpg'),
            ),
            // Fin Ejemplo Avatar
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
              child: Icon(Icons.arrow_back),
              backgroundColor: Colors.red,
              onPressed: () {
                Navigator.of(context).pop(true);
              })
        ],
      ),
    );
  }
}
